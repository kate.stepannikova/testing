from django import template

from ..models import Meta

register = template.Library()


@register.inclusion_tag("portfolio/about_text.html")
def print_meta_value(meta_key):
    try:
        return {"meta_value": Meta.objects.get(key=meta_key).value}
    except Exception as error:
        return {"meta_value": f"Meta with key <strong>\"{meta_key}\"</strong>, error: {error}"}

